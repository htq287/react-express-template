# react-express-template

The implementation of template for react-express web app

## Scripts

To start a new Create React App project with TypeScript

```sh
npx create-react-app my-app --typescript

# or

yarn create react-app react-express-template --typescript
```

To add TypeScript to a Create React App project, first install it:

```sh
npm install --save typescript @types/node @types/react @types/react-dom @types/jest

# or

yarn add typescript @types/node @types/react @types/react-dom @types/jest
```

## Uses

To runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

```sh
npm start
```

To launches the test runner in the interactive watch mode.

```sh
npm test
```

To builds the app for production to the `build` folder

```sh
npm run build
```

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
