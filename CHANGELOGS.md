# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## [0.0.2] - 2019-07-17
Changes from 0.0.1
- Server: `$ git clone https://github.com/hungtq287/react-express-template.git server`
  - Refactor source structure for `run & build`
    - Dev mode: `$ npm run dev`
    - Prod mode: `$ npm run prod`
  - `"outDir": "./dist"`
  - Load environment variables from .env file

## [0.0.1] - 2019-05-12
### Added
- Client: ``$ git clone https://github.com/hungtq287/react-express-template.git client``
  - Setup a default `React` app with `TypeScript`
  - `Dockerfile`
  - `README.md`
- Server: `$ git clone https://github.com/hungtq287/react-express-template.git server`
  - Setup a default `Express` project with `Typescript`
  - Create `Dockerfile`
  - add `README.md`
- `docker-compose.yml`
- `.travis.yml for Travis CI config`
- add global `README.md`
- add global `.gitignore`
